package app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
        System.out.println("Sample request: curl -H \"Content-Type:application/json\" -X POST http://localhost:8080/submit -d '{\"content\": \"this is my comment\"}'");
    }
}
